use std::io;
use std::net::ToSocketAddrs;

use futures::Future;

use webpki::DNSNameRef;

use tokio;
use tokio::codec::Framed;
use tokio::io::ErrorKind;
use tokio::net::TcpStream;
use tokio::prelude::*;
use tokio::runtime::Runtime;
use tokio::timer::Interval;
use tokio_rustls::{rustls::ClientConfig, TlsConnector};

use std::time::{Duration, Instant};

use mumble_codec::MumbleCodec;

use mumble_codec::Header;

use mumble_codec::Body;

use tokio_io;

use std::sync::Arc;
use tokio_channel::mpsc;

use insecure::NoCertificateVerification;
use rustls::ServerCertVerifier;

enum BusMessage<T> {
    Message(T),
    Finish,
}

fn read_loop(reader: impl AsyncRead) {
    let header: [u8; 6] = [0; 6];
    tokio_io::io::read_exact(reader, header);
}

pub fn run_murble() {
    let addr = "jugregator.org:64738"
        .to_socket_addrs()
        .unwrap()
        .next()
        .unwrap();

    let socket = TcpStream::connect(&addr);
    let mut config = ClientConfig::new();
    config
        .dangerous()
        .set_certificate_verifier(Arc::new(NoCertificateVerification {}));
    let cx = TlsConnector::from(Arc::new(config));

    let dnsname = DNSNameRef::try_from_ascii_str("jugregator.org").unwrap();

    let tls_handshake = socket.and_then(move |socket| {
        cx.connect(dnsname, socket)
            .map_err(|e| io::Error::new(io::ErrorKind::Other, e))
    });

    let (tx, rx) = mpsc::channel::<BusMessage<String>>(100);

    let ping_task = Interval::new(Instant::now(), Duration::from_secs(5))
        .for_each(|instant| {
            println!("fire; instant={:?}", instant);

            Ok(())
        }).map_err(|e| panic!("interval errored; err={:?}", e));

    let main_future = tls_handshake.and_then(|socket| {
        let codec = MumbleCodec::new();
        let framed = Framed::new(socket, codec);
        let (writer, reader) = framed.split();

        let reader_future = reader
            .for_each(move |msg| {
                println!("Got message {:?}", &msg);
                let tx = tx.clone();
                tx.send(BusMessage::Message("kek".to_string()));
                future::ok(())
            }).map_err(|_| ());

        tokio::spawn(reader_future);

        return future::ok(writer);
    });

    let rx_future = rx
        .for_each(|msg| future::ok(()))
        .map_err(|e| {
            println!("Error");
            ()
        }).map(|a| {
            //println!("{}", a);
            println!("Got message");
            ()
        });

    let sock_future = main_future
        .map_err(|e| {
            println!("Error {}", e);
            ()
        }).map(|a| {
            println!("Got {:#?}", a);
            ()
        });

    tokio::run(sock_future)
}
