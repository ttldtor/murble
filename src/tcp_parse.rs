use byteorder::{BigEndian, ReadBytesExt};
use std::io;
use std::io::Cursor;

pub fn parse_header(header: [u8; 6]) -> Result<(u16, usize), io::Error> {
    let mut rdr = Cursor::new(header);

    let packet_type = rdr.read_u16::<BigEndian>()?;

    let packet_length = rdr.read_u32::<BigEndian>()?;

    Ok((packet_type, packet_length as usize))
}

#[test]
fn parse_header_test() {
    use tcp_parse;

    let auth = [0, 1, 0, 0, 0, 76];

    let (packet_type, packet_length) = tcp_parse::parse_header(auth).unwrap();

    assert_eq!(packet_type, 1);

    assert_eq!(packet_length, 76);
}
