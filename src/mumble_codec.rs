use bytes::BytesMut;
use std::io;
use std::io::ErrorKind;
use tcp_parse;
use tokio_codec::{Decoder, Encoder};

#[derive(Debug)]
enum CodecState {
    HEADER,
    BODY,
}

#[derive(Debug)]
pub struct MumbleCodec {
    state: CodecState,
    buffer: Vec<u8>,
    readcount: usize,
    last_header: [u8; 6],
    last_body: Vec<u8>,
}

impl MumbleCodec {
    pub fn new() -> Self {
        return MumbleCodec {
            state: CodecState::HEADER,
            buffer: Vec::new(),
            readcount: 0,
            last_header: [0; 6],
            last_body: Vec::new(),
        };
    }
}

#[derive(Debug)]
pub struct Header([u8; 6]);

#[derive(Debug)]
pub struct Body(Vec<u8>);

//TODO: use later
/*
static VERSION: u16 = 0;
static UDPTUNNEL: u16 = 1;
static AUTHENTICATE: u16 = 2;
static PING: u16 = 3;
static REJECT: u16 = 4;
static SERVERSYNC: u16 = 5;
static CHANNERLREMOVE: u16 = 6;
static CHANNERLSTATE: u16 = 7;
static USERREMOVE: u16 = 8;
static USERSTATE: u16 = 9;
static BANLIST: u16 = 10;
static TEXTMESSAGE: u16 = 11;
static PERMISSIONDENIED: u16 = 12;
static ACL: u16 = 13;
static QUERYUSERS: u16 = 14;
static CRYPTSETUP: u16 = 15;
static CONTEXTACTIONMODIFY: u16 = 16;
static CONTEXTACTION: u16 = 17;
static USERLIST: u16 = 18;
static VOICETARGET: u16 = 19;
static PERMISSIONQUERY: u16 = 20;
static CODECVERSION: u16 = 21;
static USERSTATS: u16 = 22;
static REQUESTBLOB: u16 = 23;
static SERVERCONFIG: u16 = 24;
static SUGGESTCONFIG: u16 = 25;
*/

impl Decoder for MumbleCodec {
    type Item = (Header, Body);
    type Error = io::Error;

    fn decode(
        &mut self,
        src: &mut BytesMut,
    ) -> Result<Option<<Self as Decoder>::Item>, <Self as Decoder>::Error> {
        match self.state {
            CodecState::HEADER => {
                if src.len() < 6 {
                    self.readcount += src.len();
                    Ok(None)
                } else {
                    let header = src.split_to(6);
                    src.advance(6);
                    self.last_header.copy_from_slice(&header);
                    self.state = CodecState::BODY;
                    Ok(None)
                }
            }
            CodecState::BODY => {
                let (packet_type, length) =
                    tcp_parse::parse_header(self.last_header).map_err(|err| {
                        return io::Error::new(ErrorKind::InvalidInput, err);
                    })?;

                if src.len() < length {
                    return Ok(None);
                }

                let body = src.split_to(length as usize);
                src.advance(length as usize);
                return Ok(Some((Header(self.last_header), Body(body.to_vec()))));
            }
        }
    }
}

impl Encoder for MumbleCodec {
    type Item = Vec<u8>;
    type Error = io::Error;

    fn encode(
        &mut self,
        item: <Self as Encoder>::Item,
        dst: &mut BytesMut,
    ) -> Result<(), <Self as Encoder>::Error> {
        Ok(dst.extend(item))
    }
}
