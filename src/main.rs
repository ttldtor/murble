extern crate futures;
extern crate tokio;
extern crate tokio_channel;
extern crate tokio_codec;
extern crate tokio_io;
extern crate tokio_rustls;
extern crate webpki;

extern crate byteorder;
extern crate rustls;
extern crate tokio_timer;

extern crate prost;
#[macro_use]
extern crate prost_derive;
extern crate bytes;

pub mod msgs {
    include!(concat!(env!("OUT_DIR"), "/mumble_proto.rs"));
}

mod insecure;
mod mumble_codec;
mod murble_main;
mod tcp_parse;

fn main() {
    murble_main::run_murble();
}
