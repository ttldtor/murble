extern crate prost_build;

fn main() {
    print!("Building protobuf");
    prost_build::compile_protos(&["src/Mumble.proto"], &["src/"]).unwrap();
}
